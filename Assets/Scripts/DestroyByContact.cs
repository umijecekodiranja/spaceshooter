﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByContact : MonoBehaviour {

    public GameObject explosion;
    public GameObject playerExplosion;

    private GameControler gameControler;
    public int scoreValue;


    void Start()
    {
        GameObject gameControlerObject = GameObject.FindWithTag("GameController");
        if (gameControlerObject != null)
        {
            gameControler = gameControlerObject.GetComponent<GameControler>();
        }
        else
        {
            Debug.Log("Can't find 'GameController' script");
        }

    }


    void OnTriggerEnter(Collider other)
    {
     //   Debug.Log(other.name );
        if (other.name == "Boundary")
        {
            return;
        }
        Instantiate(explosion, transform.position, transform.rotation);

        if (other.tag == "Player")
        {
            Instantiate(playerExplosion, other.transform.position, other.transform.rotation);
            gameControler.GameOver();
        }

        gameControler.AddScore(scoreValue);
        Destroy(other.gameObject);
        Destroy(gameObject);
      
    }

   
}
