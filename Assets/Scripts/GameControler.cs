﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameControler : MonoBehaviour {

    public GameObject hazard;
    public Vector3 spawnValue;

    public int hazardCount;
    public float sopwnWait;
    public float startWait;
    public float waveWait;

    public GUIText scoreText;
    public GUIText restartText;
    public GUIText gameOverText;

    private int score;
    private bool restart;
    private bool gameOver;


	// Use this for initialization
	void Start () {

        restart = false;
        gameOver = false;
        restartText.text = "";
        gameOverText.text = "";
        score = 0;
        UpdateScore();
        StartCoroutine ( SpawnWaves());
    }

    void Update()
    {
        if (restart )
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
               Application.LoadLevel (Application.loadedLevel);
                SceneManager.LoadScene ("");
            }
        }
    }

    IEnumerator   SpawnWaves ()
    {
        yield return new WaitForSeconds(startWait);
        while (true)
        {
            for (int i = 0; i < hazardCount; i++)
              {
               Vector3 spawnPosition = new Vector3(Random.Range (-spawnValue.x , spawnValue.x), spawnValue.y, spawnValue.z);
               Quaternion spawnRotation = Quaternion.identity;
               Instantiate(hazard, spawnPosition, spawnRotation);
               yield return new WaitForSeconds(sopwnWait);
             }

            yield return new WaitForSeconds(waveWait);
            if (gameOver )
            {
                restartText.text = "Press 'R' for Restart";
                restart = true;
                break;
            }
        }
        
    }
	
    public void AddScore (int newScoreValue)
    {
        score += newScoreValue;
        UpdateScore();
    }

    void UpdateScore () {
        scoreText.text = "Score: " + score; 

	}

    public void GameOver()
    {
        gameOverText.text = "Game Over!";
        gameOver = true;
    }

}
