﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable ]
public class Boundary
{
    public float xMin, xMax, zMin, zMax;
}


public class PlayerControler : MonoBehaviour {

    public float speed;
    public float tilt;

    public Boundary boundary;
    private Rigidbody rigidBody;
    public GameObject shot;
    public Transform shotSpawn;

    public float fireRate;
    private float nextFire;
    private AudioSource  weaponFire;

    // Use this for initialization
    void Start () {
        rigidBody = GetComponent<Rigidbody>();
        weaponFire = GetComponent<AudioSource >();
    }
    void Update()
    {
        if(Input.GetButton("Fire1") && Time.time > nextFire )
        {

            nextFire = Time.time + fireRate;
            Instantiate(shot, shotSpawn.position, shotSpawn.rotation) ;
            weaponFire.Play();

        }
        
    }

    // Update is called once per frame
    void FixedUpdate () {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movment = new Vector3(moveHorizontal, 0.0f, moveVertical);
        rigidBody.velocity = (movment * speed);


        rigidBody.position = new Vector3(
            Mathf .Clamp(rigidBody.position.x, boundary.xMin, boundary.xMax),
            0.0f,
            Mathf.Clamp(rigidBody.position.z, boundary.zMin, boundary.zMax)
            );
        rigidBody.rotation = Quaternion.Euler(0.0f, 0.0f, rigidBody.velocity.x * - tilt);
    }
}
